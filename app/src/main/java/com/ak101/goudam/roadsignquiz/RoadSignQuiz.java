package com.ak101.goudam.roadsignquiz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class RoadSignQuiz extends AppCompatActivity {

    private static final String TAG = "RoadSignQuiz Activity"; // String used when logging error messages

    private List<String> fileNameList; // road sign file names
    private List<String> quizSignCategoriesList; // names of road sign categories in quiz
    private Map<String, Boolean> categoriesMap; // which categories are enabled
    private String correctAnswer; // correct road sign selected
    private int totalGuesses; // number of guesses made
    private int correctAnswers; // number of correct guesses
    private int guessRows; // number of rows displaying choices
    private Random random; // random number generator
    private Handler handler; // used to delay loading next road sign
    private Animation shakeAnimation; // animation for incorrect guess

    private TextView answerTextView; // displays Correct! or Incorrect!
    private TextView questionNumberTextView; // shows current question #
    private ImageView roadSignImageView; // displays a road sign
    private TableLayout buttonTableLayout; // table of answer Buttons

    // create constants for each menu id
    private final int CHOICES_MENU_ID = Menu.FIRST;
    private final int categories_MENU_ID = Menu.FIRST + 1;

    // constant for the maximum number of questions
    private final int MAX_QUESTIONS = 10;

    // gesture detector for double tap
    private GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_road_sign_quiz);

        fileNameList = new ArrayList<String>(); // list of image file names
        quizSignCategoriesList = new ArrayList<String>(); //road signs in this quiz
        categoriesMap = new HashMap<String, Boolean>(); //HashMap of categories

        guessRows = 1; //default to 1 row for choices
        random = new Random();
        handler =  new Handler();
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.incorrect_answer_shake);
        shakeAnimation.setRepeatCount(3);

        //dynamically get array of sign categories from strings.xml
        String[] categoryNames = getResources().getStringArray(R.array.categoriesList);

        //by default ,sign categories are chosen from all categories
        for(String category : categoryNames){
            categoriesMap.put(category,true);
        }

        //get references to GUI items
        questionNumberTextView = (TextView) findViewById(R.id.questionNumberTextView);
        roadSignImageView = (ImageView)findViewById(R.id.roadSignImageView);
        buttonTableLayout = (TableLayout)findViewById(R.id.optionsTableLayout);
        answerTextView = (TextView)findViewById(R.id.answerTextView);

        //set the questionNumberTextView's text
        questionNumberTextView.setText(getResources().getString(R.string.question) +
                " 1 " +getResources().getString(R.string.of) + MAX_QUESTIONS);
        resetQuiz();

        //set the mGestureDetector
        mGestureDetector = new GestureDetector(this, new SimpleGestureListener());
    }

    /**
     * SimpleGestureListener to detect double tap and open the wikipedia road signs url
     */
    private class SimpleGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.resource_url)));
            startActivity(browserIntent);
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(mGestureDetector.onTouchEvent(event)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //add 2 options to the menu
        menu.add(Menu.NONE,CHOICES_MENU_ID,Menu.NONE, R.string.choices);
        menu.add(Menu.NONE,categories_MENU_ID,Menu.NONE,R.string.categories);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // switch the menu id of the user-selected option
        switch (item.getItemId())
        {
            case CHOICES_MENU_ID:
                final String[] possibleChoices =
                        getResources().getStringArray(R.array.choicesList);

                AlertDialog.Builder choicesBuilder =
                        new AlertDialog.Builder(this);
                choicesBuilder.setTitle(R.string.choices);

                choicesBuilder.setItems(R.array.choicesList,
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int item)
                            {
                                // update guessRows to match the user's choice
                                guessRows = Integer.parseInt(
                                        possibleChoices[item].toString()) / 3;
                                resetQuiz();
                            }
                        }
                );

                AlertDialog choicesDialog = choicesBuilder.create();
                choicesDialog.show();
                return true;

            case categories_MENU_ID:
                final String[] categoryNames =
                        categoriesMap.keySet().toArray(new String[categoriesMap.size()]);

                // boolean array representing whether each category is enabled
                boolean[] categoriesEnabled = new boolean[categoriesMap.size()];
                for (int i = 0; i < categoriesEnabled.length; ++i)
                    categoriesEnabled[i] = categoriesMap.get(categoryNames[i]);

                // create an AlertDialog Builder and set the dialog's title
                AlertDialog.Builder categoriesBuilder =
                        new AlertDialog.Builder(this);
                categoriesBuilder.setTitle(R.string.categories);

                // replace _ with a space in category names
                String[] displayNames = new String[categoryNames.length];
                for (int i = 0; i < categoryNames.length; ++i)
                    displayNames[i] = categoryNames[i].replace('_', ' ');

                categoriesBuilder.setMultiChoiceItems(
                        displayNames, categoriesEnabled,
                        new DialogInterface.OnMultiChoiceClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked)
                            {
                                // include or exclude the clicked category
                                // depending on whether or not it's checked
                                categoriesMap.put(
                                        categoryNames[which].toString(), isChecked);
                            }
                        }
                );

                // resets quiz when user presses the "Reset Quiz" Button
                categoriesBuilder.setPositiveButton(R.string.reset_game,
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int button)
                            {
                                // Test for selection of at least one category
                                boolean noSelection = false;
                                for(String category: categoriesMap.keySet()){
                                    noSelection |= categoriesMap.get(category);
                                }
                                if( noSelection ){
                                    resetQuiz();
                                }
                                else{
                                    AlertDialog.Builder noSelectionAlertBuilder = new AlertDialog.Builder(RoadSignQuiz.this);
                                    noSelectionAlertBuilder.setTitle(R.string.no_item_selected_title);

                                    noSelectionAlertBuilder.setPositiveButton(R.string.ok, null);
                                    noSelectionAlertBuilder.setCancelable(true);

                                    AlertDialog noSelectionAlertDialog = noSelectionAlertBuilder.create();
                                    noSelectionAlertDialog.show();
                                }

                            }
                        }
                );

                AlertDialog categoriesDialog = categoriesBuilder.create();
                categoriesDialog.show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Reset the quiz
     */
    private void resetQuiz() {
        AssetManager assets = getAssets();
        fileNameList.clear();
        try {
            Set<String> categories = categoriesMap.keySet();
            //loop through each category
            for (String category : categories) {
                if (categoriesMap.get(category)) {
                    //if category is enabled, fetch all road signs for this category
                    Log.d(TAG, ""+category);
                    category = category.replace(' ', '_');
                    String[] paths = assets.list(category);
                    for (String path : paths) {
                        fileNameList.add(path.replace(".png", ""));
                    }
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception: While loading the image file names", e);

        }
        correctAnswers = 0;
        totalGuesses = 0;
        quizSignCategoriesList.clear();

        //add MAX_QUESTIONS random file names to the list
        int roadSignCounter = 1;
        int numberOfSigns = fileNameList.size();

        while (roadSignCounter <= MAX_QUESTIONS && roadSignCounter <= numberOfSigns) {
            Log.d(TAG, ""+roadSignCounter);
            int randomIndex = random.nextInt(numberOfSigns);
            String fileName = fileNameList.get(randomIndex);

            if (!quizSignCategoriesList.contains(fileName)) {
                quizSignCategoriesList.add(fileName);
                ++roadSignCounter;
            }

        }
        loadNextSign();
    }

    /**
     * Load the next road sign
     */
    private void loadNextSign() {
        // get file name of the next road sign and remove it from the list
        String nextImageName = quizSignCategoriesList.remove(0);

        correctAnswer = nextImageName;

        answerTextView.setText("");

        // display the current question number in quiz
        questionNumberTextView.setText(
                getResources().getString(R.string.question) + " " +
                        (correctAnswers + 1) + " " +
                        getResources().getString(R.string.of) + " " + MAX_QUESTIONS);

        // extract the category from the next image's name
        String category =
                nextImageName.substring(0, nextImageName.indexOf('-'));

        AssetManager assets = getAssets();
        InputStream stream; // used to read in road sign images

        try {
            // get an InputStream to the asset representing the next road sign
            stream = assets.open(category + "/" + nextImageName + ".png");

            // load the asset as a Drawable and display on the roadSignImageView
            Drawable roadSign = Drawable.createFromStream(stream, nextImageName);
            roadSignImageView.setImageDrawable(roadSign);
        }
        catch (IOException e) {
            Log.e(TAG, "Error loading " + nextImageName, e);
        }

        // clear the answer buttons from previous question
        for (int row = 0; row < buttonTableLayout.getChildCount(); ++row)
            ((TableRow) buttonTableLayout.getChildAt(row)).removeAllViews();

        long seed = System.nanoTime();
        Collections.shuffle(fileNameList, new Random(seed));

        // put the correct answer at the end of fileNameList later will be inserted randomly into the answer Buttons
        int correct = fileNameList.indexOf(correctAnswer);
        fileNameList.add(fileNameList.remove(correct));

        // get a reference to the LayoutInflater service
        LayoutInflater inflater = (LayoutInflater) getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        // add 3, 6, or 9 answer Buttons based on the value of guessRows
        for (int row = 0; row < guessRows; row++) {
            TableRow currentTableRow = getTableRow(row);

            // place Buttons in currentTableRow
            for (int column = 0; column < 3; column++) {
                MyButton newGuessButton =
                        (MyButton) inflater.inflate(R.layout.guess_button, null);

                // get road sign name and set it as newGuessButton's text
                String fileName = fileNameList.get((row * 3) + column);
                newGuessButton.setText(getRoadSignName(fileName));

                newGuessButton.setOnClickListener(guessButtonListener);
                currentTableRow.addView(newGuessButton);
            }
        }

        // randomly replace one Button with the correct answer
        int row = random.nextInt(guessRows);
        int column = random.nextInt(3);
        TableRow randomTableRow = getTableRow(row);
        String countryName = getRoadSignName(correctAnswer);
        ((Button) randomTableRow.getChildAt(column)).setText(countryName);
    }

    // returns the specified TableRow
    private TableRow getTableRow(int row) {
        return (TableRow) buttonTableLayout.getChildAt(row);
    }

    /**
     * gets road sign name from the file name
     * @param name
     * @return string
     */
    private String getRoadSignName(String name) {
        return name.substring(name.indexOf('-') + 1).replace('_', ' ');
    }

    /**
     * submit guess method
     * @param guessButton
     */
    private void submitGuess(Button guessButton)
    {
        String guess = guessButton.getText().toString();
        String answer = getRoadSignName(correctAnswer);
        ++totalGuesses;

        // if the guess is correct
        if (guess.equals(answer))
        {
            ++correctAnswers;

            // display "Correct!" in green text
            answerTextView.setText(answer + "!");
            answerTextView.setTextColor(
                    getResources().getColor(R.color.correct_guess));

            disableButtons(); // disable all answer Buttons

            // if the user has correctly identified MAX_QUESTIONS road signs
            if (correctAnswers == MAX_QUESTIONS)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(R.string.reset_game);

                builder.setMessage(String.format("%d %s, %.02f%% %s",
                        totalGuesses, getResources().getString(R.string.guesses_text),
                        (100 * MAX_QUESTIONS / (double) totalGuesses),
                        getResources().getString(R.string.correct_answer)));

                builder.setCancelable(false);

                builder.setPositiveButton(R.string.reset_game,
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                resetQuiz();
                            }
                        }
                );

                AlertDialog resetDialog = builder.create();
                resetDialog.show();
            }
            else // answer is correct but quiz is not over
            {
                // load the next road sign after a 1-second delay
                handler.postDelayed(
                        //anonymous inner class implementing Runnable
                        new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                loadNextSign();
                            }
                        }, 1000); // 1000 milliseconds for 1-second delay
            }
        }
        else // guess was incorrect
        {
            // play the animation
            roadSignImageView.startAnimation(shakeAnimation);

            // display "Incorrect!" in red
            answerTextView.setText(R.string.incorrect_answer);
            answerTextView.setTextColor(
                    getResources().getColor(R.color.wrong_guess));
            guessButton.setEnabled(false);
        }
    }

    // utility method that disables all answer Buttons
    private void disableButtons()
    {
        for (int row = 0; row < buttonTableLayout.getChildCount(); ++row)
        {
            TableRow tableRow = (TableRow) buttonTableLayout.getChildAt(row);
            for (int i = 0; i < tableRow.getChildCount(); ++i)
                tableRow.getChildAt(i).setEnabled(false);
        }
    }

    //called when a guess button is touched
    private View.OnClickListener guessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            submitGuess((Button)v);

        }
    };
}
